import { hot } from "react-hot-loader/root";
import { useTheme } from "hooks";
import Icon from "components/Icon";
import Button from "components/Button";
import Calculator from "components/Calculator";
import AdvPlaceCard from "components/AdvPlaceCard"
import AdvCategoryCard from "components/AdvCategoryCard"
import AdviceCard from "components/AdviceCard"

import "./App.css";

function App() {
  const theme = useTheme();

  return (
    <div className="app">
      <header>
        <div className="header container">
          <div className="header__side header__side-one">
            <Icon name="Logo" color={theme.white} size={70} />
            <div className="header__side__link">
              <Icon name="ExclamationPoint" color={theme.accents_1} size={18} />
              <a href="/some">О платформе</a>
            </div>
          </div>
          <div className="header__side header__side-two">
            <div className="header__side__link">
              <a href="/some">Для рекламных агентств</a>
            </div>
            <div className="header__side__link">
              <a href="/some">Для владельцев инвентаря</a>
            </div>
            <Button text="Вход" />
          </div>
        </div>
      </header>
      <section className="section-one">
        <div className="container">
          <div className="section-one__title">
            <h1>Рассчитайте и запустите рекламу</h1>
            <h2>которая приведёт клиентов</h2>
          </div>
          <Calculator className="flex justify-between" />
          <div className="text-filter">
            Таргетируйте рекламу <span>по линиям метро</span>
          </div>
        </div>
      </section>
      <section className="section-two">
        <div className="container">
          <div className="section-two__title">
            <h2><b>Более 150 000</b> рекламных мест</h2>
          </div>
          <div className="section-two__cards">
            <AdvPlaceCard src="./images/AdvPlaceCard-1.png" title="Видеоэкраны в метро" price="26 000"/>
            <AdvPlaceCard src="./images/AdvPlaceCard-2.png" title="Монитры в маршрутных такси" price="300"/>
            <AdvPlaceCard src="./images/AdvPlaceCard-3.png" title="Мониторы в наземном транспорте" price="200"/>
            <AdvPlaceCard src="./images/AdvPlaceCard-1.png" title="Реклама на стендах в домах" price="300"/>
            <AdvPlaceCard src="./images/AdvPlaceCard-2.png" title="Монитры в маршрутных такси" price="300"/>
          </div>
        </div>
      </section>
      <section className="section-three">
        <div className="container">
          <div className="section-three__title">
            <h2><b>Имиджевая реклама,<br />нестандартные возможности</b></h2>
          </div>
          <div className="section-three__cards">
            <AdvCategoryCard src="./images/AdvCategoryCard-1.png" title="Медиафасады"/>
            <AdvCategoryCard src="./images/AdvCategoryCard-2.png" title="Билборды по России"/>
            <AdvCategoryCard src="./images/AdvCategoryCard-3.png" title="Брендирование ступенек эскалаторов метро"/>
            <AdvCategoryCard src="./images/AdvCategoryCard-4.png" title="Нестандартные возможности в метрополитене"/>
          </div>
        </div>
      </section>
      <section className="section-four">
        <div className="container">
          <div className="section-four__title">
            <h2><b>Советы профессионалов</b></h2>
          </div>
          <div className="section-four__cards">
            <AdviceCard src="./images/AdviceCard-1.png" title="Как продвигать товары"/>
            <AdviceCard src="./images/AdviceCard-2.png" title="Почему интернет эффективнее с метро"/>
            <AdviceCard src="./images/AdviceCard-3.png" title="Гайд по использованию Qr-кода в рекламе"/>
          </div>
        </div>
      </section>
      <section className="section-six">
        <div className="container">
          <div className="section-six__title">
            <h2><b>Широкий охват за разумную цену</b></h2>
          </div>
          <div className="section-six__cards">
            <AdvPlaceCard src="./images/AdvPlaceCard-section-six-1.png" title="Видео экраны в метро" price="2 000"/>
            <AdvPlaceCard src="./images/AdvPlaceCard-section-six-2.png" title="Cтикеры в метро" price="26 000"/>
            <AdvPlaceCard src="./images/AdvPlaceCard-section-six-3.png" title="Мониторы в наземном транспорте" price="300"/>
            <AdvPlaceCard src="./images/AdvPlaceCard-section-six-4.png" title="Брендирование автобусов" price="200"/>
            <AdvPlaceCard src="./images/AdvPlaceCard-section-six-5.png" title="Диджитал экран на Савеловской" price="300"/>
          </div>
        </div>
      </section>
      <section className="section-seven">
        <div className="container">
          <div className="section-six__title">
            <h2><b>Предложения по сферам деятельности</b></h2>
          </div>
          <div className="section-seven__cards">
            <AdvCategoryCard src="./images/AdvCategoryCard-section-seven-1.png" title="Магазины"/>
            <AdvCategoryCard src="./images/AdvCategoryCard-section-seven-2.png" title="Недвижимость"/>
            <AdvCategoryCard src="./images/AdvCategoryCard-section-seven-3.png" title="Всё для дома"/>
            <AdvCategoryCard src="./images/AdvCategoryCard-section-seven-4.png" title="Авто"/>
          </div>
        </div>
      </section>
      <section className="section-eight">
        <div className="section-eight__title container">
          <h2><b>История успеха клиентов</b></h2>
        </div>
        <div className="section-seven__review">
          <img src="./images/review-1.png" alt="" />
          <img src="./images/review-2.png" alt="" />
          <img src="./images/review-3.png" alt="" />
          <img src="./images/review-4.png" alt="" />
          <img src="./images/review-5.png" alt="" />
        </div>
      </section>
    </div>
  );
}

export default hot(App);
