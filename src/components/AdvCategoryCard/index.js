import React from 'react'
import './index.css'

function AdvCategoryCard(props) {
  const title = props.title.split(' ')

  
  return (
    <div className="card">
      <div className="card__title">
        <h3>
          {
            title.map(item => {
              if (item === title[0]) {
                return <b>{item} <br /></b>
              }else{
                return item + ' '
              }
            })
          }
        </h3>
      </div>
      <img src={props.src} alt="" />
    </div>
  )
}
export default AdvCategoryCard