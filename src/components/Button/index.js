import React from "react";
import clsx from "clsx";
import Icon from "components/Icon";
import "./index.css";

const Button = ({ text, icon, fill, ...props }) => {
  return (
    <div className={clsx("button", fill && "button_fill", props.className)}>
      <button {...props}>
        {icon && <Icon size={22} name={icon} />}
        {text}
      </button>
    </div>
  );
};

export default Button;
