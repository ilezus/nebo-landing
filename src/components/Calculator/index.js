import React from "react";
import clsx from "clsx";
import Dropdown from "./Dropdown";
import Navigation from "./Navigation";
import Icon from "components/Icon";
import Button from "components/Button";
import { useTheme } from "hooks";
import "./index.css";

function Calculator(props) {
  const theme = useTheme();

  return (
    <div className="calculator-wrapper">
      <div {...props} className={clsx("calculator", props.className)}>
        <Dropdown icon="Play" label="Формат рекламы" title="Видео">
          <Navigation
            links={[
              { title: "Видео", href: "/video" },
              { title: "Нецифровые носители", href: "/video" },
            ]}
          />
        </Dropdown>
        <div className="calculator__spacer" />
        <Dropdown label="Место рекламы" title="Видеомониторы в вагонах метро">
          <div className="flex justify-between">
            <Navigation
              title="Метро"
              icon="Metro"
              links={[
                { title: "Видеоэкраны в метро", href: "/video" },
                { title: "Digital-экраны", href: "/video" },
                { title: "Метро Новосибирск", href: "/video" },
              ]}
            />
            <Navigation
              title="Внутренняя реклама"
              icon="HomeFill"
              links={[
                { title: "Мониторы в салонах красоты", href: "/video" },
                { title: "Мониторы в Почте России", href: "/video" },
                { title: "Мониторы в СДЭК", href: "/video" },
                { title: " Мониторы в ДИКСИ", href: "/video" },
              ]}
            />
          </div>
          <div className="flex justify-between margin-top-30">
            <Navigation
              title="Наружная реклама"
              icon="Home"
              links={[
                { title: "Билборды, медиафасады и др.", href: "/video" },
                { title: "Медиафасады", href: "/video" },
              ]}
            />
            <Navigation
              title="Наземный транспорт"
              icon="Bus"
              links={[
                { title: "Мониторы в наземном транспорте", href: "/video" },
                { title: "Мониторы в маршрутных такси", href: "/video" },
              ]}
            />
          </div>
        </Dropdown>
        <Button
          icon="Search"
          fill
          text="Рассчитать"
          className="calculator__button"
        />
      </div>
      <div className="calculator__description">
        <Icon name="Time" size={19} color={theme.blue_400} />
        <div>Запуск от 2 дней</div>
      </div>
    </div>
  );
}

export default Calculator;
