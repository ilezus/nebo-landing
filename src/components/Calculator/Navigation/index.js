import React from "react";
import Icon from "components/Icon";
import clsx from "clsx";
import { useTheme } from "hooks";
import "./index.css";

function Navigation({ title, icon, links }) {
  const theme = useTheme();

  return (
    <div className="calculator__navigation">
      {title && (
        <div className="calculator__navigation__title flex">
          {icon && <Icon size={20} name={icon} color={theme.blue_500} />}
          {<div>{title}</div>}
        </div>
      )}
      <div className={clsx(title && "padding")}>
        {links.map((link) => (
          <a href={link.href} key={link.href}>
            {link.title}
          </a>
        ))}
      </div>
    </div>
  );
}

export default Navigation;
