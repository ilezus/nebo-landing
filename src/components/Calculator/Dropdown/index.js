import React, { useState, useRef } from "react";
import { motion, AnimatePresence } from "framer-motion";
import Icon from "components/Icon";
import clsx from "clsx";
import { useTheme, useClickOutside } from "hooks";
import "./index.css";

function Dropdown({ className, icon, label, title, children }) {
  const theme = useTheme();
  const ref = useRef();
  const [isOpen, setOpen] = useState(false);

  const onToggleOpen = (value) => () =>
    setOpen((previousValue) => (value === "toggle" ? !previousValue : value));
  useClickOutside(ref, () => setOpen(false));

  return (
    <div className="calculator__dropdown-wrapper" ref={ref}>
      <div
        className={clsx(
          "calculator__dropdown",
          "flex",
          "align-center",
          className
        )}
        onClick={onToggleOpen("toggle")}
      >
        {icon && (
          <Icon
            size={19}
            name={icon}
            color={theme.blue_800}
            className="calculator__dropdown__icon"
          />
        )}
        <div className="calculator__dropdown__content">
          <div className="calculator__dropdown__content__label">{label}</div>
          <div className="calculator__dropdown__content__title">{title}</div>
        </div>
        <Icon
          name="ArrowBottom"
          className="calculator__dropdown__arrow"
          size={16}
          color={theme.blue_800}
        />
      </div>
      <AnimatePresence>
        {isOpen && (
          <motion.div
            initial={{ opacity: 0, transform: "translateY(15px)" }}
            animate={{ opacity: 1, transform: "translateY(5px)" }}
            exit={{ opacity: 0, transform: "translateY(5px)" }}
          >
            <div className="calculator__dropdown__children">{children}</div>
          </motion.div>
        )}
      </AnimatePresence>
    </div>
  );
}

export default Dropdown;
