import React from 'react'
import './index.css'

function AdvPlaceCard(props) {
  const title = props.title.split(' ')

  return (
    <div className="card">
      <img src={props.src} alt="" />
      <div className="card__title">
        <h4>
          {
            title.map(item => {
              if (item === title[0]) {
                return <b>{item}<br /></b>
              }else{
                return item + ' '
              }
            })
          }
        </h4>
      </div>
      <div className="card__price">
        <h1>от {props.price}₽</h1>
        <p>Подробнее</p>
      </div>
    </div>
  )
}

export default AdvPlaceCard