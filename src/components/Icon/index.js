import React from "react";
import PropTypes from "prop-types";
import * as icons from "icons";
import { useTheme } from "hooks";

const Icon = ({ size = 40, name, color = "white", ...props }) => {
  const theme = useTheme();
  const [width, height] = size instanceof Array ? size : [size, size];
  const normalizedColor =
    icons[name] instanceof Function ? "transparent" : theme[color] || color;

  let [icon, viewBox] =
    icons[name] instanceof Array
      ? [icons[name][0], { viewBox: icons[name][1] }]
      : [icons[name], {}];

  if (icons[name] instanceof Function) {
    const iconFromFunction = icons[name](color);
    icon = iconFromFunction[0];
    viewBox = { viewBox: iconFromFunction[1] };
  }

  return (
    <svg
      width={width}
      height={height}
      fill={normalizedColor}
      dangerouslySetInnerHTML={{ __html: icon }}
      {...viewBox}
      {...props}
    />
  );
};

Icon.propTypes = {
  name: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.number, PropTypes.array]),
  color: PropTypes.string,
};

export default Icon;
