import { useMemo } from "react";
import theme from "styles/theme.module.css";

export default function useTheme() {
  const currentTheme = "light"; // TODO: get from localStorage

  return useMemo(() => {
    const normalizedTheme = {};

    for (let prop in theme) {
      const slicedProp = prop.slice(0, 5);

      if (slicedProp === currentTheme) {
        normalizedTheme[prop.slice(6)] = theme[prop];
      }
    }

    return normalizedTheme;
  }, [currentTheme]);
}
